import java.awt.*;

/**
 * Created by beckmambetov on 10/17/2016.
 */
public class CalculatorTest {

    private String result = "";
    private String memory = "";
    private double firstOperand = 0;
    private double secondOperand = 0;
    private String lastOperationType = "";

    public String getResult() {
        return result;
    }

    public void clearResult() {
        result = "";
    }

    public void setLastOperationType(String type) {
        lastOperationType = type;
    }

    public String getLastOperationType() {
        return lastOperationType;
    }

    public void setFirstOperand(String value) {
        firstOperand = Double.valueOf(value);
    }

    public String getFirstOperand() {
        return String.valueOf(firstOperand);
    }

    public void setSecondOperand(String value) {
        try {
            secondOperand = Double.valueOf(value);
        }catch (Throwable ex) {

        }
    }

    public String getSecondOperand() {
        return String.valueOf(secondOperand);
    }

    public boolean isFirstOperandEmpty(){
        if (firstOperand == 0) {
            return true;
        } else {
            return false;
        }
    }

    public void concatCharacterToValue(String character) {
        result += character;
    }

    public void doBackspace(){
        if (result.length() == 0) {
            return;
        }
        result = result.substring(0,result.length()-1);
    }

    public void clearMemory() {
        memory = "";
    }

    public void saveValueInMemory(String value) {
        memory = value;
    }

    public String getValueFromMemory() {
        return memory;
    }


}
