import javax.swing.*;

public class GUI extends JDialog {
    private JPanel contentPane;
    private JTextArea textAreaResult;
    private JButton buttonMC;
    private JButton buttonMR;
    private JButton buttonCE;
    private JButton buttonSeven;
    private JButton buttonEight;
    private JButton buttonFour;
    private JButton buttonFive;
    private JButton buttonOne;
    private JButton buttonTwo;
    private JButton buttonZero;
    private JButton buttonMS;
    private JButton buttonMPlus;
    private JButton buttonMMinus;
    private JButton buttonBackspace;
    private JButton buttonC;
    private JButton buttonInvert;
    private JButton buttonSqrt;
    private JButton buttonNine;
    private JButton buttonDivide;
    private JButton buttonPercent;
    private JButton buttonSix;
    private JButton buttonMultiply;
    private JButton buttonTurn;
    private JButton buttonThree;
    private JButton buttonSubtract;
    private JButton buttonPoint;
    private JButton buttonAdd;
    private JButton buttonEquals;
    private CalculatorTest calculator = new CalculatorTest();

    private GUI() {
        setTitle("TestCalculator");
        setContentPane(contentPane);
        setModal(true);
        textAreaResult.setText(calculator.getResult());
        initListeners();
    }

    public static void main(String[] args) {
        GUI dialog = new GUI();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private void initListeners() {
        JButton[] container = new JButton[]{buttonZero, buttonOne, buttonTwo, buttonThree, buttonFour, buttonFive, buttonSix, buttonSeven, buttonEight, buttonNine};
        int counter = 0;
        for (JButton button : container) {
            int temp = counter;
            button.addActionListener(e -> {
                calculator.concatCharacterToValue(String.valueOf(temp));
                textAreaResult.setText(calculator.getResult());
            });
            counter++;
        }
        buttonPoint.addActionListener(e -> {
            calculator.concatCharacterToValue(String.valueOf("."));
            textAreaResult.setText(calculator.getResult());
        });
        buttonBackspace.addActionListener(e -> {
            calculator.doBackspace();
            textAreaResult.setText(calculator.getResult());
        });
        buttonMC.addActionListener(e -> {
            calculator.clearMemory();
        });
        buttonMR.addActionListener(e -> {
            textAreaResult.setText(calculator.getValueFromMemory());
        });
        buttonMS.addActionListener(e -> {
            calculator.saveValueInMemory(textAreaResult.getText());
            textAreaResult.setText("");
        });
        buttonMMinus.addActionListener(e -> {
            calculator.saveValueInMemory(String.valueOf(Double.valueOf(calculator.getValueFromMemory()) - Double.valueOf(textAreaResult.getText())));
        });
        buttonMPlus.addActionListener(e -> {
            calculator.saveValueInMemory(String.valueOf(Double.valueOf(calculator.getValueFromMemory()) + Double.valueOf(textAreaResult.getText())));
        });
        buttonEquals.addActionListener(e -> {
            String operand = "";
            boolean secondOperatorIsSet = false;
            if (calculator.getSecondOperand().equals("0.0")) {
                operand = calculator.getFirstOperand();
            } else {
                secondOperatorIsSet = true;
                operand = calculator.getSecondOperand();
            }
            if (!secondOperatorIsSet) {
                calculator.setSecondOperand(textAreaResult.getText());
            }
            switch (calculator.getLastOperationType()) {
                case "ADD": {
                    textAreaResult.setText(String.valueOf(Double.valueOf(operand) + Double.valueOf(textAreaResult.getText())));
                    break;
                }
                case "SUB": {
                    textAreaResult.setText(String.valueOf(Double.valueOf(operand) - Double.valueOf(textAreaResult.getText())));
                    break;
                }
                case "DIV": {
                    textAreaResult.setText(String.valueOf(Double.valueOf(operand) / Double.valueOf(textAreaResult.getText())));
                    break;
                }
                case "MUL": {
                    textAreaResult.setText(String.valueOf(Double.valueOf(operand) * Double.valueOf(textAreaResult.getText())));
                    break;
                }
            }

        });
        buttonAdd.addActionListener(e -> {
            if (!(calculator.getFirstOperand().equals("0.0") || calculator.getSecondOperand().equals("0.0"))) {
                calculator.setFirstOperand("0.0");
                calculator.setSecondOperand("0.0");
            }
            if (calculator.isFirstOperandEmpty()) {
                calculator.setFirstOperand(textAreaResult.getText());
                textAreaResult.setText("");
            } else {
                calculator.setSecondOperand(textAreaResult.getText());
                textAreaResult.setText(String.valueOf(Double.valueOf(calculator.getFirstOperand()) + Double.valueOf(textAreaResult.getText())));
            }
            calculator.setLastOperationType("ADD");
            calculator.clearResult();
        });
        buttonSubtract.addActionListener(e -> {
            if (!(calculator.getFirstOperand().equals("0.0") || calculator.getSecondOperand().equals("0.0"))) {
                calculator.setFirstOperand("0.0");
                calculator.setSecondOperand("0.0");
            }
            if (calculator.isFirstOperandEmpty()) {
                calculator.setFirstOperand(textAreaResult.getText());
                textAreaResult.setText("");
            } else {
                calculator.setSecondOperand(textAreaResult.getText());
                textAreaResult.setText(String.valueOf(Double.valueOf(calculator.getFirstOperand()) - Double.valueOf(textAreaResult.getText())));
            }
            calculator.setLastOperationType("SUB");
            calculator.clearResult();
        });
        buttonDivide.addActionListener(e -> {
            if (!(calculator.getFirstOperand().equals("0.0") || calculator.getSecondOperand().equals("0.0"))) {
                calculator.setFirstOperand("0.0");
                calculator.setSecondOperand("0.0");
            }
            if (calculator.isFirstOperandEmpty()) {
                calculator.setFirstOperand(textAreaResult.getText());
                textAreaResult.setText("");
            } else {
                calculator.setSecondOperand(textAreaResult.getText());
                textAreaResult.setText(String.valueOf(Double.valueOf(calculator.getFirstOperand()) / Double.valueOf(textAreaResult.getText())));
            }
            calculator.setLastOperationType("DIV");
            calculator.clearResult();
        });
        buttonMultiply.addActionListener(e -> {
            if (!(calculator.getFirstOperand().equals("0.0") || calculator.getSecondOperand().equals("0.0"))) {
                calculator.setFirstOperand("0.0");
                calculator.setSecondOperand("0.0");
            }
            if (calculator.isFirstOperandEmpty()) {
                calculator.setFirstOperand(textAreaResult.getText());
                textAreaResult.setText("");
            } else {
                calculator.setSecondOperand(textAreaResult.getText());
                textAreaResult.setText(String.valueOf(Double.valueOf(calculator.getFirstOperand()) * Double.valueOf(textAreaResult.getText())));
            }
            calculator.setLastOperationType("MUL");
            calculator.clearResult();
        });
        buttonCE.addActionListener(e -> {
            textAreaResult.setText("");
            calculator.setFirstOperand("0.0");
            calculator.setSecondOperand("0.0");
            calculator.clearResult();
        });
        buttonC.addActionListener(e -> {
            textAreaResult.setText("");

        });
        buttonInvert.addActionListener(e -> {
            String result = textAreaResult.getText();
            if (result.startsWith("-")) {
                result = result.substring(1,result.length());
            } else {
                result = "-" + result;
            }
            textAreaResult.setText(result);
        });
        buttonSqrt.addActionListener(e -> {
            textAreaResult.setText(String.valueOf(Math.sqrt(Double.valueOf(textAreaResult.getText()))));
        });
        buttonPercent.addActionListener(e -> {
            textAreaResult.setText(String.valueOf(Double.valueOf(textAreaResult.getText())/100));
        });
        buttonTurn.addActionListener(e -> {
            textAreaResult.setText(String.valueOf(1/Double.valueOf(textAreaResult.getText())));
        });
    }


}
