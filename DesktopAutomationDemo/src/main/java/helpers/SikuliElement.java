package helpers;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;
import pages.base.PageInstance;

import java.io.File;
import java.util.Locale;

/**
 * Created by beckmambetov on 10/25/2016.
 */
public class SikuliElement extends PageInstance {
    private String pathToImage;

    public SikuliElement() {
        if (osType == null) {
            String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
            if ((OS.contains("mac")) || (OS.contains("darwin"))) {
                PageInstance.osType = "MacOS";
            } else if (OS.contains("win")) {
                PageInstance.osType = "Win";
            } else if (OS.contains("nux")) {
                PageInstance.osType = "Linux";
            }
        }
    }

    public void click() throws FindFailed {
        switch (PageInstance.osType) {
            case "Linux":
                screen.click(pathToImage.split("\\.")[0] + "_Ubuntu." + pathToImage.split("\\.")[1]);
                break;
            case "Win": {
                screen.click(pathToImage);
                break;
            }
        }
        screen.mouseMove(20, 20);
    }

    public void setPathToImage(String pathToImage) {
        if (!PageInstance.osType.equals("Win")) {
            pathToImage = pathToImage.replace("\\", File.separator);
        }
        this.pathToImage = pathToImage;
    }

    public boolean isExist() {
        switch (PageInstance.osType) {
            case "Linux": {
                return screen.exists(pathToImage.split("\\.")[0] + "_Ubuntu." + pathToImage.split("\\.")[1]) != null;
            }
            case "Win": {
                return screen.exists(pathToImage) != null;
            }
        }
        return false;
    }
}
