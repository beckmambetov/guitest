package pages.current;

import helpers.SikuliElement;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;

import java.util.List;

/**
 * Created by beckmambetov on 10/25/2016.
 */
public class SikuliMainPage {

    private Screen currentScreen;

    private SikuliElement button1;
    private SikuliElement button2;
    private SikuliElement button3;
    private SikuliElement button4;
    private SikuliElement button5;
    private SikuliElement button6;
    private SikuliElement button7;
    private SikuliElement button8;
    private SikuliElement button9;
    private SikuliElement button0;
    private SikuliElement buttonAdd;
    private SikuliElement buttonEquals;
    private SikuliElement resultFieldFirstTest;
    private SikuliElement resultFieldSecondTest;

    public SikuliMainPage() {
        this.currentScreen = new Screen();
    }

    public boolean checkFirstTest() {
        return resultFieldFirstTest.isExist();
    }

    public boolean checkSecondTest() {
        return resultFieldSecondTest.isExist();
    }

    public void enterNumbers(String number) throws FindFailed {
        char[] x = number.toCharArray();
        for (char temp : x) {
            switch (temp) {
                case '0': {
                    button0.click();
                    break;
                }
                case '1': {
                    button1.click();
                    break;
                }
                case '2': {
                    button2.click();
                    break;
                }
                case '3': {
                    button3.click();
                    break;
                }
                case '4': {
                    button4.click();
                    break;
                }
                case '5': {
                    button5.click();
                    break;
                }
                case '6': {
                    button6.click();
                    break;
                }
                case '7': {
                    button7.click();
                    break;
                }
                case '8': {
                    button8.click();
                    break;
                }
                case '9': {
                    button9.click();
                    break;
                }
            }
        }
    }

    public void sum(String firstPart, String secondPart) throws FindFailed {
        enterNumbers(firstPart);
        buttonAdd.click();
        enterNumbers(secondPart);
    }

    public void finishEquation() throws FindFailed {
        buttonEquals.click();
    }



    public void setButton1(SikuliElement button1) {
        this.button1 = button1;
    }

    public void setButton2(SikuliElement button2) {
        this.button2 = button2;
    }

    public void setButton3(SikuliElement button3) {
        this.button3 = button3;
    }

    public void setButton4(SikuliElement button4) {
        this.button4 = button4;
    }

    public void setButton5(SikuliElement button5) {
        this.button5 = button5;
    }

    public void setButton6(SikuliElement button6) {
        this.button6 = button6;
    }

    public void setButton7(SikuliElement button7) {
        this.button7 = button7;
    }

    public void setButton8(SikuliElement button8) {
        this.button8 = button8;
    }

    public void setButton9(SikuliElement button9) {
        this.button9 = button9;
    }

    public void setButton0(SikuliElement button0) {
        this.button0 = button0;
    }

    public void setResultFieldFirstTest(SikuliElement resultFieldFirstTest) {
        this.resultFieldFirstTest = resultFieldFirstTest;
    }

    public void setButtonAdd(SikuliElement buttonAdd) {
        this.buttonAdd = buttonAdd;
    }

    public void setButtonEquals(SikuliElement buttonEquals) {
        this.buttonEquals = buttonEquals;
    }

    public void setResultFieldSecondTest(SikuliElement resultFieldSecondTest) {
        this.resultFieldSecondTest = resultFieldSecondTest;
    }
}
