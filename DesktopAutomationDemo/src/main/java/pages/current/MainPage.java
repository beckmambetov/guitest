package pages.current;

import pages.base.PageInstance;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by beckmambetov on 10/11/2016.
 */
public class MainPage extends PageInstance {

    /*private String button1;
    private String button2;
    private String button3;
    private String button4;
    private String button5;
    private String button6;
    private String button7;
    private String button8;
    private String button9;
    private String button0;
    private String buttonAdd;
    private String buttonEquals;
    private String menuView;
    private String radioButtonHexadecimal;


    public void enterNumbers(String number) {
        char[] x = number.toCharArray();
        for (char temp : x) {
            switch (temp) {
                case '0': {
                    provider.click(button0);
                    break;
                }
                case '1': {
                    provider.click(button1);
                    break;
                }
                case '2': {
                    provider.click(button2);
                    break;
                }
                case '3': {
                    provider.click(button3);
                    break;
                }
                case '4': {
                    provider.click(button4);
                    break;
                }
                case '5': {
                    provider.click(button5);
                    break;
                }
                case '6': {
                    provider.click(button6);
                    break;
                }
                case '7': {
                    provider.click(button7);
                    break;
                }
                case '8': {
                    provider.click(button8);
                    break;
                }
                case '9': {
                    provider.click(button9);
                    break;
                }
            }
        }
    }

    public String getResult() {
        List<String> results = Arrays.stream(provider.getObjectList()).filter(p -> p.contains("lbl")).collect(Collectors.toList());
        return results.get(3).replace("lbl", "");
    }

    public void sum(String firstPart, String secondPart) {
        enterNumbers(firstPart);
        provider.click(buttonAdd);
        enterNumbers(secondPart);
    }

    public void swtichView(String name) throws InterruptedException {
        if (name.contains(" ")) {
            int spaceQuantity = name.split(" ").length - 1;
            int[] spaceIndexes = new int[spaceQuantity];
            int lastSpaceIndex = -1;
            for (int i = 0; i < spaceQuantity; i++) {
                spaceIndexes[i] = name.indexOf(" ", lastSpaceIndex + 1);
                lastSpaceIndex = spaceIndexes[i];
            }
            char[] charForm = name.toCharArray();
            for (int x : spaceIndexes) {
                charForm[x + 1] = (String.valueOf(charForm[x + 1])).toLowerCase().toCharArray()[0];
            }
            name = String.valueOf(charForm).replace(" ", "");
        }
        provider.click(menuView);
        try {
            provider.click("mnu" + name);
        } catch (Throwable ex) {
            String x = name;
            provider.click(menuView);
            provider.click(menuView);
            String temp = Arrays.stream(provider.getObjectList()).filter(p -> p.contains("mnu" + x)).collect(Collectors.toList()).get(0);
            provider.click(menuView);
            provider.click(menuView);
            provider.click(temp);
            provider.getObjectList();

        }
        Thread.sleep(1000);
    }

    public String getViewType() {
        String[] elements = provider.getObjectList();
        boolean isScientific = false;
        boolean isProgrammer = false;
        boolean isStatistic = false;
        for (String element : elements) {
            if (element.equals("rbtnDegrees")) {
                isScientific = true;
            }
            if (element.equals("rbtnQuadrupleWord")) {
                isProgrammer = true;
            }
            if (element.equals("btnClearalldatasetvalues")) {
                isStatistic = true;
            }
        }
        if (!(isScientific || isProgrammer || isStatistic)) {
            return "Standard";
        }
        if (isScientific) {
            return "Scientific";
        }
        if (isProgrammer) {
            return "Programmer";
        }
        if (isStatistic) {
            return "Statistic";
        }
        return null;
    }

    public String getModeType() {
        String[] elements = provider.getObjectList();
        boolean isUnitConversion = false;
        boolean isDateCalculation = false;
        for (String element : elements) {
            if (element.equals("lblSelectthetypeofunityouwanttoconvert")) {
                isUnitConversion = true;
            }
            if (element.contains("lblSelectthedatecalculationyouwant")) {
                isDateCalculation = true;
            }
        }
        if (!(isUnitConversion || isDateCalculation)) {
            return "Basic";
        }
        if (isUnitConversion) {
            return "Unit Conversion";
        }
        if (isDateCalculation) {
            return "Date Calculation";
        }
        return null;
    }

    public void changeCountSystem(String arg0) {
        switch (arg0) {
            case "Hex": {
                provider.click(radioButtonHexadecimal);
            }
        }
    }

    public String finishEquation() {
        provider.click(buttonEquals);
        return getResult();
    }

    public void setButton1(String button1) {
        this.button1 = button1;
    }

    public void setButton2(String button2) {
        this.button2 = button2;
    }

    public void setButton3(String button3) {
        this.button3 = button3;
    }

    public void setButton4(String button4) {
        this.button4 = button4;
    }

    public void setButton5(String button5) {
        this.button5 = button5;
    }

    public void setButton6(String button6) {
        this.button6 = button6;
    }

    public void setButton7(String button7) {
        this.button7 = button7;
    }

    public void setButton8(String button8) {
        this.button8 = button8;
    }

    public void setButton9(String button9) {
        this.button9 = button9;
    }

    public void setButton0(String button0) {
        this.button0 = button0;
    }

    public void setButtonAdd(String buttonAdd) {
        this.buttonAdd = buttonAdd;
    }

    public void setButtonEquals(String buttonEquals) {
        this.buttonEquals = buttonEquals;
    }

    public void setMenuView(String menuView) {
        this.menuView = menuView;
    }

    public void setRadioButtonHexadecimal(String radioButtonHexadcimal) {
        this.radioButtonHexadecimal = radioButtonHexadcimal;
    }*/
}
