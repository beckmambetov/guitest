package pages.base;


import arp.CucumberArpReport;
import arp.ReportClasses.Step;
import arp.ReportService;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.sikuli.script.Screen;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

/**
 * Created by logovskoy on 12/3/2015.
 */

@ContextConfiguration("classpath:cucumber-context.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public abstract class PageInstance {
    public static Screen screen;
    public static Process p;
    public static CucumberArpReport arpClient;
    public static PageInstance currentPageObject;
    public static String osType;

    public static boolean checkIfFurtherStepsAreNeeded() throws TimeoutException, InterruptedException {
        try {
            List<Step> passedSteps = CucumberArpReport.getReport().CurrentTestCase.Steps.stream().filter(p -> p.Actions != null && p.Actions.size() > 0).collect(Collectors.toList());
            if (passedSteps.stream().anyMatch(p -> !p.Status.equals("pass"))) {
                try {
                    ReportService.ReportAction("Step is blocked by further step fails", false);
                    CucumberArpReport.nextStep();
                    return true;
                } catch (Throwable e) {
                }
            }

            return false;
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return false;
    }


}
