package testUtils; /**
 * Created by logovskoy on 12/3/2015.
 */


import com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver;
import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.junit.Cucumber;
import helpers.SystemHelper;
import arp.CucumberArpReport;
import arp.ReportClasses.Report;
import arp.ReportClasses.Step;
import arp.ReportClasses.Test;
import arp.ReportService;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import pages.base.PageInstance;

import static helpers.SystemHelper.PROJECTKEY;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory.setArpReportClient;


/**
 * @RunWith Class with this annotation will run a Cucumber Feature. The class should be empty without any fields or methods.
 * @CucumberOptions This annotation provides the same options as the cucumber command line.
 * @params format What formatter(s) to use.
 * @params features The paths to the feature(s).
 * @params glue Where to look for glue code (stepdefs and hooks).
 * @params tags Identify scenarios or features to run.
 */
@RunWith(Cucumber.class)

@CucumberOptions(format = {"pretty",
        "html:target/cucumber-htmlreport",
        //"json:target/cucumber-report.json",
        "junit:target/cucumber-junit-report/cuc.xml"}
        , glue = {"stepdefinition", "testUtils"}
        , features = {"src/test/resources"}
        , tags = {"@High"}
)
public class TestRunner extends PageInstance {


    @BeforeClass
    public static void BeforeClass() {
        try {
            File dir = new File("src/test/resources");
            List<File> features = Arrays.stream(dir.listFiles()).filter(p -> p.getName().contains(".feature")).collect(Collectors.toList());
            BufferedReader br = new BufferedReader(new FileReader(features.get(0)));
            String featureName = "";
            while (true) {
                String stringFromFile = br.readLine();
                if (stringFromFile.contains("Feature:")) {
                    featureName = stringFromFile.replace("Feature:", "");
                    while (featureName.startsWith(" ") || featureName.endsWith(" ")) {
                        featureName = featureName.trim();
                    }
                    break;
                }
            }
            CucumberArpReport.open(PROJECTKEY, featureName);
            ReportService.turnOnJUnitLogging();
            Report report = CucumberArpReport.getReport();
            CucumberArpReport.setReport(report);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void CreateReport() throws Exception {
        CucumberArpReport.getReport();
    }
}
