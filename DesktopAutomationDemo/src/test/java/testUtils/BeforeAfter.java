package testUtils;

import helpers.SystemHelper;
import arp.ReportService;
import org.sikuli.basics.Settings;
import org.sikuli.script.Screen;
import pages.base.PageInstance;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import arp.CucumberArpReport;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory.setArpReportClient;
import static helpers.SystemHelper.MAINWINDOWHANDLER;
import static helpers.SystemHelper.Reset_Values;

/**
 * Created by logovskoy on 12/3/2015.
 */
public class BeforeAfter extends PageInstance {

    @Before
    public void setUp(Scenario scenario) throws IOException {
        Reset_Values();
        try {
            setArpReportClient(new CucumberArpReport());
            if (!scenario.getId().startsWith(CucumberArpReport.getTestSuiteName().toLowerCase())) {
                ReportService.close();
                CucumberArpReport.open("B26AAC2A-4D91-4231-9315-9FFA4B6DCD16", scenario.getId());
            }

            if (p == null || !p.isAlive()) {
                switch (PageInstance.osType) {
                    case "Linux":
                        p = new ProcessBuilder(new String[] {"java", "-jar", SystemHelper.FILE}).start();
                        break;
                    case "Win": {
                        p = new ProcessBuilder(new String[] {"java", "-jar", SystemHelper.FILE}).start();
                        break;
                    }
                }

            }
            Thread.sleep(3000);
            Settings.MinSimilarity = 0.99;
            PageInstance.screen = new Screen();

            CucumberArpReport.addTestToTestSuite(scenario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown(Scenario scenario) throws Exception {
        try {
            p.destroy();
            p = null;
            CucumberArpReport.decideTestStatus();
            ReportService.FinishTest();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }
}