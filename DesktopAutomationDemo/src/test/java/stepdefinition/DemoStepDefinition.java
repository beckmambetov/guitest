package stepdefinition;

import arp.CucumberArpReport;
import arp.ReportService;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.current.MainPage;
import pages.current.SikuliMainPage;

import java.util.concurrent.TimeoutException;

/**
 * Created by beckmambetov on 9/26/2016.
 */
public class DemoStepDefinition extends PageInstance {

    @Autowired
    SikuliMainPage mp;

    @Given("^I have open \"([^\"]*)\" application$")
    public void I_have_open_application(String arg0) throws Exception {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            //ReportService.ReportAction("Window name matches expected", provider.getWindowName().equals("*"+arg0+"*"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I enter \"([^\"]*)\"$")
    public void iEnter(String arg0) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            mp.enterNumbers(arg0);
            ReportService.ReportAction("Values were entered", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see 1234567890 in result$")
    public void iSee1234567890InResult() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.ReportAction("Values were entered", mp.checkFirstTest());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see 444 in result$")
    public void iSee444InResult() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.ReportAction("Values were entered", mp.checkSecondTest());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I sum \"([^\"]*)\" with \"([^\"]*)\"$")
    public void iSumWith(String arg0, String arg1) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            mp.sum(arg0,arg1);
            mp.finishEquation();
            ReportService.ReportAction("Values were summed",true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    /*@When("^I change view to \"([^\"]*)\"$")
    public void iSelectChangeViewTo(String arg0) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            mp.swtichView(arg0);
            ReportService.ReportAction("View was changed",true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that \"([^\"]*)\" view was enabled$")
    public void iSeeThatViewWasEnabled(String arg0) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.ReportAction("Type is as expected",mp.getViewType().equals(arg0));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I change mode to \"([^\"]*)\"$")
    public void iSelectChangeModeTo(String arg0) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            mp.swtichView(arg0);
            ReportService.ReportAction("Mode was changed",true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that \"([^\"]*)\" mode was enabled$")
    public void iSeeThatModeWasEnabled(String arg0) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.ReportAction("Type is as expected",mp.getModeType().equals(arg0));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I change count system to \"([^\"]*)\"$")
    public void iChangeCountSystemTo(String arg0) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            mp.changeCountSystem(arg0);
            ReportService.ReportAction("System count was changed",true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }*/
}
