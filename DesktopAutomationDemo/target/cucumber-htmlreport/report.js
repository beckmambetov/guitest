$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Demo.feature");
formatter.feature({
  "line": 2,
  "name": "Demo",
  "description": "",
  "id": "demo",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Feature_123"
    }
  ]
});
formatter.before({
  "duration": 3014749312,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Enter values",
  "description": "",
  "id": "demo;enter-values",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@High"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I have open \"Calculator\" application",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I enter \"1234567890\"",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I see 1234567890 in result",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Calculator",
      "offset": 13
    }
  ],
  "location": "DemoStepDefinition.I_have_open_application(String)"
});
formatter.result({
  "duration": 260380238,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1234567890",
      "offset": 9
    }
  ],
  "location": "DemoStepDefinition.iEnter(String)"
});
formatter.result({
  "duration": 23212308672,
  "status": "passed"
});
formatter.match({
  "location": "DemoStepDefinition.iSee1234567890InResult()"
});
formatter.result({
  "duration": 1001697120,
  "status": "passed"
});
formatter.after({
  "duration": 1850865,
  "status": "passed"
});
formatter.before({
  "duration": 3006955025,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Sum",
  "description": "",
  "id": "demo;sum",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 10,
      "name": "@High"
    }
  ]
});
formatter.step({
  "line": 12,
  "name": "I have open \"Calculator\" application",
  "keyword": "Given "
});
formatter.step({
  "line": 13,
  "name": "I sum \"123\" with \"321\"",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "I see 444 in result",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Calculator",
      "offset": 13
    }
  ],
  "location": "DemoStepDefinition.I_have_open_application(String)"
});
formatter.result({
  "duration": 647964,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123",
      "offset": 7
    },
    {
      "val": "321",
      "offset": 18
    }
  ],
  "location": "DemoStepDefinition.iSumWith(String,String)"
});
formatter.result({
  "duration": 13885090926,
  "status": "passed"
});
formatter.match({
  "location": "DemoStepDefinition.iSee444InResult()"
});
formatter.result({
  "duration": 847042716,
  "status": "passed"
});
formatter.after({
  "duration": 209237,
  "status": "passed"
});
});